package com.filipturek.transactiongeneratorspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransactionGeneratorSpringbootApplication {
	public static void main(String[] args) {
		SpringApplication.run(TransactionGeneratorSpringbootApplication.class, args);
	}

}
