package com.filipturek.transactiongeneratorspringboot.controllers;

import com.filipturek.transactiongeneratorspringboot.JSONReader;
import com.filipturek.transactiongeneratorspringboot.generatortools.model.Transaction;
import com.filipturek.transactiongeneratorspringboot.generatortools.saver.JsonSaver;
import com.filipturek.transactiongeneratorspringboot.generatortools.saver.Saver;
import com.filipturek.transactiongeneratorspringboot.generatortools.saver.XMLSaver;
import com.filipturek.transactiongeneratorspringboot.generatortools.saver.YamlSaver;
import com.filipturek.transactiongeneratorspringboot.generatortools.utils.TransactionGenerator;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Slf4j
@RestController
public class GeneratorController {

    final static Logger logger = LoggerFactory.getLogger(TransactionGenerator.class);
    private final String url = "https://csv-items-generator.herokuapp.com/items";
    private final String acceptHeader = "application/json";

    private TransactionGenerator transactionGenerator;

    public GeneratorController(TransactionGenerator transactionGenerator) {
        this.transactionGenerator = transactionGenerator;
    }

    Saver saver;

    @RequestMapping(value = "/transactions", method = RequestMethod.GET, produces = {"application/json"})
    public String getTransactionJSON(
            @RequestParam(value = "customerIds", required = false) String customerIds,
            @RequestParam(value = "dateRange", required = false) String dateRange,
            @RequestParam(value = "itemsCount", required = false) String itemsCount,
            @RequestParam(value = "itemsQuantity", required = false) String itemsQuantity,
            @RequestParam(value = "eventsCount", required = false) String eventsCount
    ) {

        if (dateRange != null) {
            dateRange = dateRange.replace("\"", "");
        }

        System.out.println(customerIds);

        HashMap<String, String> params = new HashMap<>();
        params.put("customerIds", customerIds);
        params.put("dateRange", dateRange);
        params.put("itemsCount", itemsCount);
        params.put("itemsQuantity", itemsQuantity);
        params.put("eventsCount", eventsCount);


        ArrayList<Transaction> transactions = generate(params);
        if (!transactions.isEmpty() && !transactions.equals("")) {
            saver = new JsonSaver();

            try {
                return saver.save(transactions);
            } catch (JAXBException e) {
                e.printStackTrace();
                return "JAXBException: " + e;
            }
        } else {
            logger.warn("Transaction generator failed.");
            return "Transaction generator failed.";
        }
    }

    @RequestMapping(value = "/transactions", method = RequestMethod.GET, produces = {"application/xml"})
    public ArrayList<Transaction>  getTransactionXML(
            @RequestParam(value = "customerIds", required = false) String customerIds,
            @RequestParam(value = "dateRange", required = false) String dateRange,
            @RequestParam(value = "itemsCount", required = false) String itemsCount,
            @RequestParam(value = "itemsQuantity", required = false) String itemsQuantity,
            @RequestParam(value = "eventsCount", required = false) String eventsCount
    ) {

        if (dateRange != null) {
            dateRange = dateRange.replace("\"", "");
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("customerIds", customerIds);
        params.put("dateRange", dateRange);
        params.put("itemsCount", itemsCount);
        params.put("itemsQuantity", itemsQuantity);
        params.put("eventsCount", eventsCount);


        ArrayList<Transaction> transactions = generate(params);
        if (!transactions.isEmpty() && !transactions.equals("")) {
            logger.info("...::Done::...");
            return transactions;
        } else {
            logger.warn("Transaction generator failed.");
            return null;
        }

    }

    @RequestMapping(value = "/transactions", method = RequestMethod.GET, produces = {"application/yaml"})
    public String getTransactionYAML(
            @RequestParam(value = "customerIds", required = false) String customerIds,
            @RequestParam(value = "dateRange", required = false) String dateRange,
            @RequestParam(value = "itemsCount", required = false) String itemsCount,
            @RequestParam(value = "itemsQuantity", required = false) String itemsQuantity,
            @RequestParam(value = "eventsCount", required = false) String eventsCount
    ) {

        if (dateRange != null) {
            dateRange = dateRange.replace("\"", "");
        }

        System.out.println(customerIds);

        HashMap<String, String> params = new HashMap<>();
        params.put("customerIds", customerIds);
        params.put("dateRange", dateRange);
        params.put("itemsCount", itemsCount);
        params.put("itemsQuantity", itemsQuantity);
        params.put("eventsCount", eventsCount);

        ArrayList<Transaction> transactions = generate(params);
        if (!transactions.isEmpty() && !transactions.equals("")) {
            saver = new YamlSaver();

            try {
                return saver.save(transactions);
            } catch (JAXBException e) {
                e.printStackTrace();
                return "JAXBException: " + e;
            }
        } else {
            logger.warn("Transaction generator failed.");
            return "Transaction generator failed.";
        }

    }

    private ArrayList<Transaction> generate(HashMap<String, String> params) {
        JSONArray jsonArray = null;
        List<String> itemList = new ArrayList<>();
        try {
            jsonArray = JSONReader.readJsonFromUrl(url, acceptHeader);

            for (int i = 0; i < jsonArray.length(); i++) {
                String name = jsonArray.getJSONObject(i).getString("name");
                String price = jsonArray.getJSONObject(i).getString("price");
                itemList.add(name + "," + price);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return transactionGenerator.generate(params, itemList);

    }

}
