package com.filipturek.transactiongeneratorspringboot.generatortools.saver;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.filipturek.transactiongeneratorspringboot.generatortools.model.Transaction;
import lombok.Data;
import java.util.List;

@Data
@JacksonXmlRootElement(localName = "List")
public class TransactionXmlWrapper {

    @JacksonXmlElementWrapper(useWrapping = true)
    @JacksonXmlProperty(localName = "transaction")
    private List<Transaction> transactions;

    public TransactionXmlWrapper(List<Transaction> transactions) {
        this.transactions = transactions;
    }
}
