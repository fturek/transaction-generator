package com.filipturek.transactiongeneratorspringboot.generatortools.saver;

import com.filipturek.transactiongeneratorspringboot.generatortools.model.Transaction;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Representer;

import java.io.StringWriter;
import java.util.ArrayList;

@Service
public class YamlSaver implements Saver {

    @Override
    public String save(ArrayList<Transaction> transactions) {
        Representer representer = new Representer();
        representer.addClassTag(Transaction.class, new Tag("!transaction"));
        Yaml yaml = new Yaml(representer, new DumperOptions());
        StringWriter sw = new StringWriter();
        yaml.dump(transactions, sw);

        return sw.toString();
    }
}
