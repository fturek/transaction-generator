package com.filipturek.transactiongeneratorspringboot.generatortools.saver;

import com.filipturek.transactiongeneratorspringboot.generatortools.model.Transaction;

import javax.xml.bind.JAXBException;
import java.util.ArrayList;

public interface Saver {
    String save(ArrayList<Transaction> transactions) throws JAXBException;
}
