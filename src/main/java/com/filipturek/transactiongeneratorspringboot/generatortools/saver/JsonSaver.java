package com.filipturek.transactiongeneratorspringboot.generatortools.saver;

import com.filipturek.transactiongeneratorspringboot.generatortools.model.Transaction;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class JsonSaver implements Saver {

    @Override
    public String save(ArrayList<Transaction> transactions) {

        String result = "";

        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting().serializeNulls();
        Gson gson = builder.create();
        result = gson.toJson(transactions);

        return result;
    }

}
