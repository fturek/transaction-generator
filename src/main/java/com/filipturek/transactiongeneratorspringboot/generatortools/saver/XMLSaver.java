package com.filipturek.transactiongeneratorspringboot.generatortools.saver;

import com.filipturek.transactiongeneratorspringboot.generatortools.model.Transaction;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.util.ArrayList;

@Service
public class XMLSaver implements Saver {

    @Override
    public String save(ArrayList<Transaction> transactions) throws JAXBException {

        TransactionXmlWrapper xmlTransactionWrapper = new TransactionXmlWrapper(transactions);
        JAXBContext jaxbContext = JAXBContext.newInstance(TransactionXmlWrapper.class);
        Marshaller marshaller = jaxbContext.createMarshaller();
        StringWriter sw = new StringWriter();

        marshaller.marshal(xmlTransactionWrapper, sw);

        return sw.toString();
    }
}
