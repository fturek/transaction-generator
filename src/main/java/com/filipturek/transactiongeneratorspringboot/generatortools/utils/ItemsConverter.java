package com.filipturek.transactiongeneratorspringboot.generatortools.utils;

import com.filipturek.transactiongeneratorspringboot.generatortools.model.Item;
import com.filipturek.transactiongeneratorspringboot.generatortools.model.ParsedParameters;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class ItemsConverter {

    public ArrayList<Item> readItems(List<String> itemsFile, String sep, ParsedParameters parsedParameters) {

        String separator = sep;
        String singleItem;
        ArrayList<Item> items = new ArrayList<>();

        int countItems = Randomizer.getInt(parsedParameters.getItemsCountFrom(), parsedParameters.getItemsCountTo());
        for (int i = 0; i < countItems; i++) {
            singleItem = itemsFile.get(ThreadLocalRandom.current().nextInt(0, itemsFile.size()));
            String[] itemValues = singleItem.split(separator);
            Item item = new Item();
            item.name = itemValues[0].replaceAll("\"", "");
            item.quantity = Randomizer.getInt(parsedParameters.getItemsQuantityFrom(), parsedParameters.getItemsQuantityTo());
            item.price = Float.parseFloat(itemValues[1]);
            items.add(item);
        }
        return items;
    }


}
