package com.filipturek.transactiongeneratorspringboot.generatortools.utils;

import com.filipturek.transactiongeneratorspringboot.generatortools.date.DateHelper;
import com.filipturek.transactiongeneratorspringboot.generatortools.model.RawParameters;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class OptionParser {

    final Logger logger = LoggerFactory.getLogger(OptionParser.class);
    private HashMap<String,String> arguments;

    private String customerIdsFromTo;
    private String dateRange;
    private String itemsCountFromTo;
    private String itemsQuantityFromTo;
    private String eventsCount;

    public void setArguments(HashMap<String,String> arguments) {
        this.arguments = arguments;
    }

    public void parseArgs() {
        logger.info("Parsing args.");
        customerIdsFromTo = arguments.containsKey("customerIds") && arguments.get("customerIds") != null ? arguments.get("customerIds") : "1:2";
        dateRange = arguments.containsKey("dateRange") && arguments.get("dateRange") != null ? arguments.get("dateRange") : DateHelper.prepareDateToday();
        itemsCountFromTo = arguments.containsKey("itemsCount") && arguments.get("itemsCount") != null ? arguments.get("itemsCount") : "1:2";
        itemsQuantityFromTo = arguments.containsKey("itemsQuantity") && arguments.get("itemsQuantity") != null ? arguments.get("itemsQuantity") : "1:2";
        eventsCount = arguments.containsKey("eventsCount") && arguments.get("eventsCount") != null ? arguments.get("eventsCount") : "10";
    }

    public RawParameters createRawParameters(){
        logger.info("Creating raw params");
        RawParameters rawParameters = new RawParameters();
        rawParameters.setCustomerIdsFromTo(customerIdsFromTo);
        rawParameters.setDateRange(dateRange);
        rawParameters.setItemsCountFromTo(itemsCountFromTo);
        rawParameters.setItemsQuantityFromTo(itemsQuantityFromTo);
        rawParameters.setEventsCount(eventsCount);
        return rawParameters;
    }

}
