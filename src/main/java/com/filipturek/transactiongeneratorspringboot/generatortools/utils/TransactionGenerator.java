package com.filipturek.transactiongeneratorspringboot.generatortools.utils;

import com.filipturek.transactiongeneratorspringboot.generatortools.model.ParsedParameters;
import com.filipturek.transactiongeneratorspringboot.generatortools.model.RawParameters;
import com.filipturek.transactiongeneratorspringboot.generatortools.model.Transaction;
import com.filipturek.transactiongeneratorspringboot.generatortools.saver.JsonSaver;
import com.filipturek.transactiongeneratorspringboot.generatortools.saver.Saver;
import com.filipturek.transactiongeneratorspringboot.generatortools.saver.XMLSaver;
import com.filipturek.transactiongeneratorspringboot.generatortools.saver.YamlSaver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class TransactionGenerator {

    final static Logger logger = LoggerFactory.getLogger(TransactionGenerator.class);

    private OptionParser optionParser;
    private OptionsValidator optionsValidator;

    RawParameters rawParameters;
    ParsedParameters parsedParameters;

    public TransactionGenerator(OptionParser optionParser, OptionsValidator optionsValidator) {
        this.optionParser = optionParser;
        this.optionsValidator = optionsValidator;
    }

    public ArrayList<Transaction> generate(HashMap<String, String> args, List<String> itemList) {
        logger.info("Generate.");
        optionParser.setArguments(args);
        optionParser.parseArgs();
        rawParameters = optionParser.createRawParameters();

        optionsValidator.setRawParameters(rawParameters);
        boolean optionsOk = optionsValidator.validateOptions();
        if (!optionsOk) {
            logger.warn("Transaction generator failed.");
            return null;
        }
        parsedParameters = optionsValidator.getParsedParameters();

        int howMany = Integer.parseInt(parsedParameters.getEventsCount());
        ArrayList<Transaction> transactions = new ArrayList<>();
        for (int i = 0; i < howMany; i++) {
            transactions.addAll(TransactionParser.createTransaction(parsedParameters, i, itemList));
        }

        return transactions;
    }
}
