package com.filipturek.transactiongeneratorspringboot.generatortools.utils;

import com.filipturek.transactiongeneratorspringboot.generatortools.model.Item;
import com.filipturek.transactiongeneratorspringboot.generatortools.model.ParsedParameters;
import com.filipturek.transactiongeneratorspringboot.generatortools.model.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class TransactionParser {

    final static Logger logger = LoggerFactory.getLogger(TransactionGenerator.class);

    public static ArrayList<Transaction> createTransaction(ParsedParameters parsedParameters, int id, List<String> itemList) {

        String itemsCsvFile = parsedParameters.getItemsFile();
        ItemsConverter itemsConverter = new ItemsConverter();
        ArrayList<Item> items = itemsConverter.readItems(itemList, ",", parsedParameters);

        ArrayList<Transaction> transactions = new ArrayList<Transaction>();

        Transaction transaction = new Transaction();
        transaction.id = id;
        transaction.timestamp = parsedParameters.getDateRange();
        transaction.customer_id = Randomizer.getInt(parsedParameters.getCustomerIdFrom(), parsedParameters.getCustomerIdTo());
        transaction.items = items;
        transaction.sum = items.stream().mapToDouble(item -> item.quantity * item.price).sum();
        transactions.add(transaction);


        return transactions;
    }
}
