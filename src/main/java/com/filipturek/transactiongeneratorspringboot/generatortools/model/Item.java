package com.filipturek.transactiongeneratorspringboot.generatortools.model;

import jdk.nashorn.internal.ir.annotations.Immutable;
import lombok.Data;

@Immutable
@Data
public class Item {
    public String name;
    public int quantity;
    public float price;
}
